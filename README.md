#EMPLOYEE - SERVICE #

## Startimi i aplikacionit: ##
* ```docker-compose up```
* ```mvn spring-boot:run```

## Consumer ##
Konsumeri konsumon mesazhe nga tre topic qe jane:
* employee-general-information-topic
* employee-qualification-topic
* employee-project-topic




## Api ##
###General Information ###
* insert ```/api/employee/general-information```
* ``curl -H "Content-Type: application/json" -H "X-EVENT-TYPE: INSERT" -d '{"passportID":"passID","name":"name","surname":"surname","email":"email@yahoo.com","age":26}' -X POST http://localhost:8080/api/employee/general-information``
* update ```/api/employee/{id}/general-information```
* ``curl -H "Content-Type: application/json" -H "X-EVENT-TYPE: UPDATE" -d '{"passportID":"passID","name":"name","surname":"surname","email":"updatedEmail@yahoo.com","age":26}' -X POST http://localhost:8080/api/employee/general-information``
* delete ```/api/employee/{id}/general-information```
* ``curl -H "X-EVENT-TYPE: DELETE"  -X DELETE http://localhost:8080/api/employee/5f8ee49502077b02a6880003/general-information``
### Qualification ###
* insert ```/api/employee/{id}/qualification```
* update ```/api/employee/{id}/qualification/{qualificationID}```
* delete ```/api/employee/{id}/qualification/{qualificationID}```
### Project ###
* insert ```/api/employee/{id}/project```
* delete ```/api/employee/{id}/project/{projectID}```


