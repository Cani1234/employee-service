package com.icub.task.employee.gateway;

import com.icub.task.employee.commons.exception.EmployeeNotFoundException;
import com.icub.task.employee.commons.exception.ExceptionResponse;
import com.icub.task.employee.commons.exception.ProjectNotFoundException;
import com.icub.task.employee.commons.exception.QualificationNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class EmployeeControllerAdvice extends ResponseEntityExceptionHandler {


    @ExceptionHandler(EmployeeNotFoundException.class)
    protected ResponseEntity<Object> handleEmployeeNotFound(EmployeeNotFoundException ex){
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST,ex.getMessage()), HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(ProjectNotFoundException.class)
    protected ResponseEntity<Object> handleEmployeeNotFound(ProjectNotFoundException ex){
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST,ex.getMessage()), HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(QualificationNotFoundException.class)
    protected ResponseEntity<Object> handleEmployeeNotFound(QualificationNotFoundException ex){
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST,ex.getMessage()), HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleIllegalArgument(QualificationNotFoundException ex){
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST,ex.getMessage()), HttpStatus.BAD_REQUEST);

    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers,
                                                                     HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE," media type is not supported."), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST,getRequiredFields(ex)), HttpStatus.BAD_REQUEST);
    }





    private Map<String,String> getRequiredFields(MethodArgumentNotValidException ex){
        Map<String,String> errors= new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(e ->{
            errors.put(e.getField(),e.getDefaultMessage());
        });
        return errors;
    }



}
