package com.icub.task.employee.commons.exception;

public class QualificationNotFoundException extends RuntimeException{

    public QualificationNotFoundException(String message){
        super(message);
    }
}
