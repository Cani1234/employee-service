package com.icub.task.employee.service.impl;

import com.icub.task.employee.commons.entity.Employee;
import com.icub.task.employee.commons.event.GeneralInformationEvent;
import com.icub.task.employee.commons.event.ProjectEvent;
import com.icub.task.employee.commons.event.QualificationEvent;
import com.icub.task.employee.commons.exception.EmployeeNotFoundException;
import com.icub.task.employee.commons.exception.QualificationNotFoundException;
import com.icub.task.employee.repository.EmployeeRepository;
import com.icub.task.employee.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SuccessCallback;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public void generalInformationHandler(GeneralInformationEvent event){
        Employee employee;
        switch (event.getType()){
            case INSERT:
                employee = Employee.builder().generalInformation(event.getGeneralInformation()).build();
                this.insert(employee);
                break;
            case UPDATE:
                employee = findById(event.getEmployeeID());
                employee.setGeneralInformation(event.getGeneralInformation());
                this.insert(employee);
                break;
            case DELETE:
                employee = findById(event.getEmployeeID());
                this.delete(employee.getId());
                break;
            default: throw new UnsupportedOperationException();
        }
    }

    @Override
    public void qualificationHandler(QualificationEvent event){
        Employee employee = findById(event.getEmployeeID());
        List<Employee.Qualification> qualifications;
        switch (event.getType()){
            case INSERT:
                if (employee.getQualifications() != null) employee.getQualifications().add(event.getQualification());
                    else employee.setQualifications(Arrays.asList(event.getQualification()));
                this.insert(employee);
                break;
            case UPDATE:
                this.insert(updateQualification(employee,event));
            case DELETE:
                qualifications = employee.getQualifications()
                        .stream().filter(q->q.getQualificationID().equals(event.getQualification().getQualificationID()))
                        .collect(Collectors.toList());
                employee.setQualifications(qualifications);
                this.insert(employee);
                break;
            default: throw new UnsupportedOperationException();
        }
    }

    @Override
    public void projectHandler(ProjectEvent event){
        Employee employee = findById(event.getEmployeeID());
        List<Employee.Project> projects;
        switch (event.getType()){
            case INSERT:
                if (employee.getProjects() != null)
                    employee.getProjects().add(event.getProject());
                else employee.setProjects(Arrays.asList(event.getProject()));
                this.insert(employee);
                break;
            case DELETE:
                projects = employee.getProjects().stream()
                        .filter(p->!p.getProjectID().equals(event.getProject().getProjectID())).collect(Collectors.toList());
                employee.setProjects(projects);
                this.insert(employee);
                break;
            default: throw new UnsupportedOperationException();
        }

    }

    @Override
    public Employee insert(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void delete(String id) {
        findById(id);
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee findById(String id) {
        return employeeRepository.findById(id).orElseThrow(()->new EmployeeNotFoundException("Employee with id "+id+" does not exist"));
    }

    @Override
    public void kafkaCallback(ListenableFuture listenableFuture){
        SuccessCallback<SendResult<Object,Object>> successCallback = result -> log.info("PRODUCED RECORD:  [topic: {}] , [payload:{}]",result.getRecordMetadata().topic(),result.getProducerRecord().value().toString());
        FailureCallback failureCallback = ex -> log.error("Error thrown on producer: ", ex.getLocalizedMessage());
        listenableFuture.addCallback(successCallback,failureCallback);
    }

    @Override
    public Employee findByIdAndQualification(String id, String qualificationID){
        Employee emp = findById(id);
        emp.getQualifications().stream()
                .filter(q->q.getQualificationID().equals(qualificationID))
                .findFirst().orElseThrow(()-> new QualificationNotFoundException("Qualification with id: "+qualificationID+" does not exist"));
        return emp;
    }


    public Employee updateQualification(Employee employee,QualificationEvent event){
        List<Employee.Qualification> qualifications;
        employee.getQualifications()
                .stream().filter(q->q.getQualificationID().equals(event.getQualification().getQualificationID()))
                .findFirst().orElseThrow(()-> new QualificationNotFoundException("Qualification with id: "+event.getQualification().getQualificationID()+" does not exist"));
        qualifications = employee.getQualifications().stream()
                .filter(q->!q.getQualificationID().equals(event.getQualification().getQualificationID()))
                .collect(Collectors.toList());
        qualifications.add(event.getQualification());
        employee.setQualifications(qualifications);
        employee = this.insert(employee);
        employee.getQualifications().add(event.getQualification());
        return employee;
    }







}
